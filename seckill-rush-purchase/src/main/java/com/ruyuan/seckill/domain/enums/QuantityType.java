package com.ruyuan.seckill.domain.enums;

/**
 * 库存数类型
 */
public enum QuantityType {

    /**
     * 实际的库存，包含了待发货的
     */
    actual,

    /**
     * 可以售的库存，不包含待发货的
     */
    enable

}
