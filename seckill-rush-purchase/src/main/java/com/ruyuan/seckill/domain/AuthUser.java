package com.ruyuan.seckill.domain;

import java.util.List;

public interface AuthUser {

    List<String> getRoles();

    void setRoles(List<String> roles);
}
