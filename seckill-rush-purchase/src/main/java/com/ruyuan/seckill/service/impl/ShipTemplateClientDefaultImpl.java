package com.ruyuan.seckill.service.impl;


import com.ruyuan.seckill.domain.vo.ShipTemplateVO;
import com.ruyuan.seckill.service.ShipTemplateClient;
import com.ruyuan.seckill.service.ShipTemplateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShipTemplateClientDefaultImpl implements ShipTemplateClient {

    @Autowired
        private ShipTemplateManager shipTemplateManager;

    @Override
    public ShipTemplateVO get(Integer id) {
        return shipTemplateManager.getFromCache(id);
    }

    // @Override
    // public List<String> getScripts(Integer id) {
    //
    //
    //     return shipTemplateManager.getScripts(id);
    // }
    //
    // @Override
    // public void cacheShipTemplateScript(ShipTemplateVO shipTemplateVO) {
    //
    //     this.shipTemplateManager.cacheShipTemplateScript(shipTemplateVO);
    // }
}
